<?php

namespace Services;

use App\Models\Product;
use App\Models\User;

class ProductService
{
    public function getModel()
    {
        return Product::class;
    }

    public function getall()
    {
        return $this->getModel()->all();
    }

    public function findById($id)
    {
        return $this->getModel()->find($id);
    }

    public function store($data)
    {
        return $this->getModel()->store($data);
    }

    public function update($data, $id)
    {
        return $this->getModel()->find($id)->update($data);
    }

    public function delete($id)
    {
        return $this->getModel()->delete($id);
    }
}

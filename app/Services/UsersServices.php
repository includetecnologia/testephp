<?php

namespace App\Services;

use App\Services\Services;
use App\Models\User;

class UsersServices implements Services
{

    public function getModel()
    {
        return new User();
    }

    public function getall()
    {

        return $this->getModel()->get();
    }

    public function findById($id)
    {
        return $this->getModel()->find($id);
    }

    public function store($data)
    {
        return $this->getModel()->store($data);
    }

    public function update($data, $id)
    {
        return $this->getModel()->find($id)->update($data);
    }

    public function delete($id)
    {
        return $this->getModel()->delete($id);
    }
}

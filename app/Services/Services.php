<?php

namespace App\Services;

interface Services
{
    public function getModel();
    public function getall();
    public function findById($id);
    public function store($data);
    public function update($data, $id);
    public function delete($id);
}

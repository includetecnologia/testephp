<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserUpdadeRequest;
use Illuminate\Http\Request;
use App\Services\UsersServices;

class UsersController extends Controller
{

    private UsersServices $services;

    public function __construct(UsersServices $services)
    {
        $this->services = $services;
    }

    public function index()
    {
        $users = $this->services->getall();
        return view('users.index', compact('users'));
    }

    public function create()
    {
        return view('users.create');
    }

    public function edit($id)
    {
        $user = $this->services->findById($id);
        return view('users.edit', compact('user'));
    }

    public function store(UserCreateRequest $request)
    {
        $user = $this->services->store($request->all());
        $response = [
            'message' => __('messages.created'),
            'data' => $user->toArray(),
        ];
        if ($request->wantsJson()) {
            return response()->json($response);
        }
        return redirect()->back()->with('message', $response['message']);
    }

    public function update($id, UserUpdadeRequest $request)
    {
        $user = $this->services->update($request->all(), $id);
        $response = [
            'message' => __('messages.updated'),
            'data' => $user->toArray(),
        ];
        if ($request->wantsJson()) {
            return response()->json($response);
        }
        return redirect()->back()->with('message', $response['message']);
    }

    public function delete($id)
    {
        $deleted = $this->services->delete($id);
        $response = [
            'message' => __('messages.deleted'),
            'deleted'=>$deleted
        ];
        if (request()->wantsJson()) {
            return response()->json($response);
        }
        return redirect()->back()->with('message', $response['message']);
    }


}

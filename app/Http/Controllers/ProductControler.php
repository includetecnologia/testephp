<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Services\ProductService;

class ProductControler extends Controller
{
    private ProductService $services;

    public function __construct(ProductService $services)
    {
        $this->services = $services;
    }

    public function index()
    {
        $products = $this->services->getall();
        return view('products.index', compact('products'));
    }

    public function create()
    {
        return view('products.create');
    }

    public function edit($id)
    {
        $product = $this->services->findById($id);
        return view('products.edit', compact('product'));
    }

    public function store(UserCreateRequest $request)
    {
        $product = $this->services->store($request->all());
        $response = [
            'message' => __('messages.created'),
            'data' => $product->toArray(),
        ];
        if ($request->wantsJson()) {
            return response()->json($response);
        }
        return redirect()->back()->with('message', $response['message']);
    }

    public function update($id, UserUpdadeRequest $request)
    {
        $product = $this->services->update($request->all(), $id);
        $response = [
            'message' => __('messages.updated'),
            'data' => $product->toArray(),
        ];
        if ($request->wantsJson()) {
            return response()->json($response);
        }
        return redirect()->back()->with('message', $response['message']);
    }

    public function delete($id)
    {
        $deleted = $this->services->delete($id);
        $response = [
            'message' => __('messages.deleted'),
            'deleted'=>$deleted
        ];
        if (request()->wantsJson()) {
            return response()->json($response);
        }
        return redirect()->back()->with('message', $response['message']);
    }
}
